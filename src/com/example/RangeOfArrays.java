package com.example;

import java.util.Scanner;

public class RangeOfArrays {
    int[] array;
    int size;

    public void initializeArray(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter the Size of the Array:- ");
        size=scanner.nextInt();
        array=new int[size];
        System.out.println("Enter the ArrayElements:- ");
        for (int iterate = 0; iterate < size; iterate++) {
            array[iterate]=scanner.nextInt();
        }
    }

    public int rangeFinder(){
        int minimum=array[0];
        int maximum=array[0];
        for (int iterate = 1; iterate < size; iterate++) {
            if(array[iterate] < minimum){
                minimum=array[iterate];
            }
            else if(array[iterate] > maximum){
                maximum=array[iterate];
            }
        }
        return maximum-minimum;
    }
}
