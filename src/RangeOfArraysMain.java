import com.example.RangeOfArrays;

public class RangeOfArraysMain {
    public static void main(String[] args) {
        RangeOfArrays rangeOfArrays=new RangeOfArrays();
        rangeOfArrays.initializeArray();
        System.out.println("Range Between Array:- "+rangeOfArrays.rangeFinder());
    }
}